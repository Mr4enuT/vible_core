<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VIBLE TEAM license that is
 * available in LICENSE.txt file.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @package     Vible_Core
 * @author      Taras Lishchynskiy <4enutl@gmail.com>
 * @copyright   2017 VIBLE TEAM
 * @license     LICENSE.txt GNU License
 */

namespace Vible\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * AbstractData core helper
 */
class AbstractData extends AbstractHelper
{
    /**
     * @param $field
     * @param null  $storeId
     * @return mixed
     */
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}

<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VIBLE TEAM license that is
 * available in LICENSE.txt file.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @package     Vible_Core
 * @author      Taras Lishchynskiy <4enutl@gmail.com>
 * @copyright   2017 VIBLE TEAM
 * @license     LICENSE.txt GNU License
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Vible_Core',
    __DIR__
);
